import sys
import csv
import json
import os


def parse(name, mode):
    data = []
    classes_filename = name + '/classes.txt' 
    if os.path.exists(classes_filename):
        labels = open(classes_filename, 'r').read().splitlines()
    else:
        labels = ["very negative", "negative", "neutral", "positive", "very positive"]
    question = "Is this sentence " + ", ".join(labels[:-1]) + ", or " + labels[-1] + "?"
    with open(name + '/' + mode + '.csv', 'r') as f:
        csv_reader = csv.reader(f, delimiter=',', quotechar='"')
        csv_reader = csv.reader(f, delimiter=',', quotechar='"')
        for row in csv_reader:
            label = labels[int(row[0])-1]
            context = ' '.join(row[1:])
            data.append({"paragraphs": [{"context": context, "qas":[{"question":question, "answers":[{"text": label}]}]}]})
    with open(name + '_to_squad-' + mode + '-v2.0.json', 'w') as f:
        json.dump({"data": data}, f)

for name in ["ag_news_csv", "yelp_review_full_csv", "amazon_review_full_csv", "dbpedia_csv", "yahoo_answers_csv"]:
    for mode in ['train', 'test']:
        parse(name, mode)
